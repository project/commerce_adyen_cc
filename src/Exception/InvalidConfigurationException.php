<?php

namespace Drupal\commerce_adyen_cc\Exception;

/**
 * Thrown when an invalid configuration is detected.
 */
class InvalidConfigurationException extends \RuntimeException {

}
