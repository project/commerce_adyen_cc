<?php

namespace Drupal\commerce_adyen_cc\Exception;

/**
 * Thrown when 3ds2 is required.
 */
class ThreeDS2Exception extends \RuntimeException {

  /**
   * The data.
   *
   * @var mixed
   */
  public $data;

  /**
   * Construct a 3ds2 exception.
   *
   * @param string $message
   *   The message.
   * @param int $code
   *   The code.
   * @param \Throwable|null $previous
   *   The previous throwable.
   * @param array|null $data
   *   The 3ds2 data.
   */
  public function __construct(string $message = '', int $code = 0, \Throwable $previous = NULL, ?array $data = NULL) {
    parent::__construct($message, $code, $previous);
    $this->data = $data;
  }

  /**
   * Get the associated data.
   *
   * @return array|null
   *   The data.
   */
  public function getData(): ?array {
    return $this->data;
  }

}
