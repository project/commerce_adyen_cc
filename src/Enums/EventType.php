<?php

namespace Drupal\commerce_adyen_cc\Enums;

/**
 * The Event types for Adyen api events.
 */
class EventType {

  /**
   * All payments start with creating a session.
   */
  public const SESSION = 'session';

  /**
   * Authenticate.
   *
   * This is called when payment information is provided
   * to determine if 3ds2 needs to be handled.
   */
  public const AUTHENTICATE = 'authenticate';

  /**
   * Authentication verification.
   *
   * If 3ds2 is required, then this verifies the response from the user.
   */
  public const AUTHENTICATE_3DS2_VERIFY = 'authenticate_3ds2_verify';

  /**
   * If 3ds2 IS required, this event processes the $0 authorization.
   */
  public const AUTHORIZE_ZERO_DOLLAR_3DS2_REQUIRED = 'authorize_zero_dollar_3ds2_required';

  /**
   * If 3ds2 is NOT required, this event processes the $0 authorization.
   */
  public const AUTHORIZE_ZERO_DOLLAR_3DS2_NOT_REQUIRED = 'authorize_zero_dollar_3ds2_not_required';

  /**
   * Handles the authorize event.
   */
  public const AUTHORIZE = 'authorize';

  /**
   * Handles the capture event.
   */
  public const CAPTURE = 'capture';

  /**
   * Handles the refund event.
   */
  public const REFUND = 'refund';

  /**
   * Handles the void event.
   */
  public const VOID = 'void';

}
