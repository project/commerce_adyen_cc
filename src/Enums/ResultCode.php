<?php

namespace Drupal\commerce_adyen_cc\Enums;

/**
 * The Adyen result codes.
 */
class ResultCode {

  public const AUTHENTICATION_NOT_REQUIRED = 'AuthenticationNotRequired';

  public const AUTHORIZED = 'Authorised';

  public const AUTHENTICATION_FINISHED = 'AuthenticationFinished';

  public const REFUSED = 'Refused';

  public const ERROR = 'Error';

  public const CANCELLED = 'Cancelled';

  public const CHALLENGE_SHOPPER = 'ChallengeShopper';

  public const IDENTIFY_SHOPPER = 'IdentifyShopper';

  public const RECEIVED = 'received';

}
