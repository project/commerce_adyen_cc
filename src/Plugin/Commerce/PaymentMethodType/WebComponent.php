<?php

namespace Drupal\commerce_adyen_cc\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_adyen_cc\AdyenHelper;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;
use Drupal\entity\BundleFieldDefinition;

/**
 * Provides the credit card payment method type.
 *
 * @CommercePaymentMethodType(
 *   id = "adyen_cc_web_component",
 *   label = @Translation("Payment (Adyen)"),
 * )
 */
class WebComponent extends PaymentMethodTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method) {
    $card_type = AdyenHelper::mapCreditCardType($payment_method->get('card_type')->getString());
    $card_number = !$payment_method->get('card_number')->isEmpty() ? $payment_method->get('card_number')->getString() : 'unknown';
    $args = [
      '@card_type' => $card_type,
      '@card_number' => $card_number,
    ];
    if (empty(array_filter($args))) {
      $label = $this->t('Payment (Adyen)');
    }
    else {
      $label = $this->t('Payment (Adyen): @card_type ending in @card_number', $args);
    }
    return $label;
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = parent::buildFieldDefinitions();

    $fields['card_type'] = BundleFieldDefinition::create('list_string')
      ->setLabel($this->t('Card type'))
      ->setDescription($this->t('The credit card type.'))
      ->setRequired(FALSE)
      ->setSetting('allowed_values_function', [
        AdyenHelper::class,
        'getCreditCardTypes',
      ]);

    $fields['card_number'] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('Card number'))
      ->setDescription($this->t('The last few digits of the credit card number'))
      ->setRequired(FALSE);

    // card_exp_month and card_exp_year are not required because they might
    // not be known (tokenized non-reusable payment methods).
    $fields['card_exp_month'] = BundleFieldDefinition::create('integer')
      ->setLabel($this->t('Card expiration month'))
      ->setDescription($this->t('The credit card expiration month.'))
      ->setSetting('size', 'tiny');

    $fields['card_exp_year'] = BundleFieldDefinition::create('integer')
      ->setLabel($this->t('Card expiration year'))
      ->setDescription($this->t('The credit card expiration year.'))
      ->setSetting('size', 'small');

    return $fields;
  }

}
