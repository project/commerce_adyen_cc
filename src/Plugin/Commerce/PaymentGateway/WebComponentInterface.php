<?php

namespace Drupal\commerce_adyen_cc\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsAuthorizationsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Drupal\profile\Entity\ProfileInterface;

/**
 * Provides the interface for the Web Component payment gateway.
 */
interface WebComponentInterface extends OnsitePaymentGatewayInterface, SupportsAuthorizationsInterface, SupportsRefundsInterface {

  /**
   * Get the session data.
   *
   * @return array
   *   The session data.
   */
  public function createPaymentSession(): array;

  /**
   * Creates a payment method with the given payment details.
   *
   * @param array $payment_details
   *   The gateway-specific payment details provided by the payment method form
   *   for on-site gateways, or the incoming request for off-site gateways.
   * @param \Drupal\profile\Entity\ProfileInterface $billing_profile
   *   The billing profile.
   */
  public function tokenize(array $payment_details, ProfileInterface $billing_profile): void;

  /**
   * Perform authorization.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function authorize(PaymentInterface $payment): void;

  /**
   * Verify the 3ds2 response.
   *
   * @param array $data
   *   The data.
   */
  public function verify3ds2(array $data): void;

  /**
   * Return an array of allowed country codes.
   *
   * @return array
   *   The allowed country codes.
   */
  public function getBillingAddressAllowedCountries(): array;

}
