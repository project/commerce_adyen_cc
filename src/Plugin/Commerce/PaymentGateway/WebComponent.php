<?php

namespace Drupal\commerce_adyen_cc\Plugin\Commerce\PaymentGateway;

use Adyen\AdyenException;
use Adyen\Client;
use Adyen\Environment;
use Adyen\Service\Checkout;
use Drupal\commerce_adyen_cc\AdyenHelper;
use Drupal\commerce_adyen_cc\Enums\EventType;
use Drupal\commerce_adyen_cc\Enums\ResultCode;
use Drupal\commerce_adyen_cc\Event\AdyenCCEvents;
use Drupal\commerce_adyen_cc\Event\ApiDataEvent;
use Drupal\commerce_adyen_cc\Exception\InvalidConfigurationException;
use Drupal\commerce_adyen_cc\Exception\ThreeDS2Exception;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\CreditCard;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\InvalidRequestException;
use Drupal\commerce_payment\Exception\InvalidResponseException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Exception\SoftDeclineException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\commerce_price\Price;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Utility\Error;
use Drupal\profile\Entity\ProfileInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Web Component payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "adyen_cc_web_component",
 *   label = "Adyen Web Component",
 *   display_label = "Adyen",
 *   forms = {
 *     "add-payment-method" = "Drupal\commerce_adyen_cc\PluginForm\WebComponent\PaymentMethodAddForm",
 *   },
 *   js_library = "commerce_adyen_cc/adyen_cc",
 *   payment_method_types = {"adyen_cc_web_component"},
 *   requires_billing_information = TRUE,
 * )
 */
class WebComponent extends OnsitePaymentGatewayBase implements WebComponentInterface {

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The Adyen gateway used for making API calls.
   *
   * @var \Adyen\Client
   */
  protected $api;

  /**
   * The order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * The current path.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The Private User Temporary Storage.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected $privateTempStore;

  /**
   * The  module extension list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The version.
   *
   * @var string
   */
  protected $version;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition,
    );
    $instance->eventDispatcher = $container->get('event_dispatcher');
    $instance->currentPath = $container->get('path.current');
    $instance->requestStack = $container->get('request_stack');
    $instance->routeMatch = $container->get('current_route_match');
    /** @var \Drupal\Core\TempStore\PrivateTempStoreFactory $private_temp_store_factory */
    $private_temp_store_factory = $container->get('tempstore.private');
    $instance->privateTempStore = $private_temp_store_factory->get('commerce_adyen_cc');
    $instance->moduleExtensionList = $container->get('extension.list.module');
    /** @var \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory */
    $logger_factory = $container->get('logger.factory');
    $instance->logger = $logger_factory->get('commerce_adyen_cc');
    $instance->fileSystem = $container->get('file_system');
    $instance->dateFormatter = $container->get('date.formatter');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'api_key' => '',
      'client_key' => '',
      'merchant_account' => '',
      'timeout' => 30,
      'capture' => TRUE,
      'live_endpoint_url_prefix' => '',
      'log_api_requests' => FALSE,
      'save_api_requests' => FALSE,
      'include_additional_amount' => FALSE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['live_endpoint_url_prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Live endpoint url prefix'),
      '#default_value' => $this->configuration['live_endpoint_url_prefix'],
      '#description' => $this->t('The prefix only. Do not provide a full or partial url. Obtain this from your Adyen account. Follow these <a href="https://docs.adyen.com/development-resources/live-endpoints/" target="_blank">directions</a> to locate your prefix.'),
      '#states' => [
        'enabled' => [
          ':input[name="configuration[adyen_cc_web_component][mode]"]' => ['value' => 'live'],
        ],
        'required' => [
          ':input[name="configuration[adyen_cc_web_component][mode]"]' => ['value' => 'live'],
        ],
      ],
      '#attributes' => [
        'pattern' => '^[^./ ]+$',
        'title' => $this->t('The live prefix only. Do not include a partial or full url.'),
      ],
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API key'),
      '#default_value' => $this->configuration['api_key'],
      '#maxlength' => 256,
      '#required' => TRUE,
    ];

    $form['client_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client key'),
      '#default_value' => $this->configuration['client_key'],
      '#required' => TRUE,
    ];

    $form['merchant_account'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant account'),
      '#default_value' => $this->configuration['merchant_account'],
      '#required' => TRUE,
    ];

    $form['timeout'] = [
      '#type' => 'number',
      '#min' => -1,
      '#max' => 60,
      '#step' => 1,
      '#title' => $this->t('Timeout'),
      '#description' => $this->t('The timeout in seconds.'),
      '#default_value' => $this->configuration['timeout'],
      '#required' => TRUE,
    ];

    $form['capture'] = [
      '#type' => 'radios',
      '#title' => $this->t('Transaction mode'),
      '#options' => [
        TRUE => $this->t('Authorize and honor the <strong>capture delay</strong> setting in the Adyen Customer Area Settings > Account settings > General > Capture delay'),
        FALSE => $this->t('Authorize only (requires manual capture after checkout)'),
      ],
      '#description' => $this->t('Note: a capture delay setting of Manual Capture is <strong>NOT</strong> supported.'),
      '#default_value' => (int) $this->configuration['capture'],
    ];

    $form['include_additional_amount'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include additional amount'),
      '#default_value' => $this->configuration['include_additional_amount'],
      '#description' => $this->t('Normally, a $0 authorization is performed to verify the payment is valid. For 3ds2 payments, a $0 amount is displayed in the 3ds2 component. Enabling this option displays the actual order amount in the 3ds2 component, with the tradeoff that, rather than a $0 auth being performed, Adyen performs an auth+cancel instead.'),
    ];

    $form['log_api_requests'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log API requests'),
      '#default_value' => $this->configuration['log_api_requests'],
      '#description' => $this->t('Log API requests.'),
    ];

    $form['save_api_requests'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Save API requests'),
      '#default_value' => $this->configuration['save_api_requests'],
      '#description' => $this->t('Save API requests to the private file system. Each request/response will be saved as a json file.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['api_key'] = $values['api_key'];
      $this->configuration['client_key'] = $values['client_key'];
      $this->configuration['merchant_account'] = $values['merchant_account'];
      $this->configuration['timeout'] = $values['timeout'];
      $this->configuration['capture'] = $values['capture'];
      $this->configuration['live_endpoint_url_prefix'] = $values['live_endpoint_url_prefix'];
      $this->configuration['log_api_requests'] = $values['log_api_requests'];
      $this->configuration['save_api_requests'] = $values['save_api_requests'];
      $this->configuration['include_additional_amount'] = $values['include_additional_amount'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    $payment_information = $this->getPaymentInformation();
    $payment_method->setReusable($this->getOrder()->getCustomer()->isAuthenticated());
    $payment_method->setRemoteId($payment_information['remote_id']);
    $payment_method->set('card_type', $payment_information['card_type'] ?? 'Unknown');
    $payment_method->set('card_number', $payment_information['card_number']);
    $payment_method->set('card_exp_month', $payment_information['card_exp_month']);
    $payment_method->set('card_exp_year', $payment_information['card_exp_year']);
    $expires = CreditCard::calculateExpirationTimestamp($payment_information['card_exp_month'], $payment_information['card_exp_year']);
    $payment_method->setExpiresTime($expires);
    $payment_method->save();
    $this->deletePaymentSession();
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    // Delete the remote record.
    try {
      // @todo implement.
      // The Adyen SDK does not implement the deletion of the stored payment.
      // We would need to either use the Rest API directly or submit a patch
      // to the SDK to support stored payment deletion.
    }
    catch (AdyenException $e) {
      $this->handleException($e, []);
    }
    // Delete the local entity.
    $payment_method->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    $this->authorize($payment);
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['authorization']);
    // If not specified, capture the entire amount.
    $amount = $amount ?: $payment->getAmount();

    try {
      $remote_id = $payment->getRemoteId();
      $currency_code = $amount->getCurrencyCode();
      $api_data = [
        'paymentPspReference' => $remote_id,
        'merchantAccount' => $this->configuration['merchant_account'],
        'amount' => [
          'currency' => $currency_code,
          'value' => AdyenHelper::getMinorUnits($amount),
        ],
      ];

      $api_data = $this->dispatchApiData(EventType::CAPTURE, $api_data, $payment);

      $service = new Checkout($this->getApi());
      $result = $service->captures($api_data);
      $this->handleResponse($api_data, $result, [ResultCode::RECEIVED]);
    }
    catch (AdyenException $e) {
      $this->handleException($e, $api_data ?? []);
    }

    $payment->setState('completed');
    $payment->setAmount($amount);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    $this->assertPaymentState($payment, ['authorization']);

    try {
      $remote_id = $payment->getRemoteId();
      $api_data = [
        'paymentPspReference' => $remote_id,
        'merchantAccount' => $this->configuration['merchant_account'],
      ];

      $api_data = $this->dispatchApiData(EventType::VOID, $api_data, $payment);

      $service = new Checkout($this->getApi());
      $result = $service->cancels($api_data);
      $this->handleResponse($api_data, $result, [ResultCode::RECEIVED]);
    }
    catch (AdyenException $e) {
      $this->handleException($e, $api_data ?? []);
    }

    $payment->setState('authorization_voided');
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $this->assertRefundAmount($payment, $amount);

    try {
      $remote_id = $payment->getRemoteId();
      $currency_code = $amount->getCurrencyCode();
      $api_data = [
        'paymentPspReference' => $remote_id,
        'merchantAccount' => $this->configuration['merchant_account'],
        'amount' => [
          'currency' => $currency_code,
          'value' => AdyenHelper::getMinorUnits($amount),
        ],
      ];

      $api_data = $this->dispatchApiData(EventType::REFUND, $api_data, $payment);

      $service = new Checkout($this->getApi());
      $result = $service->refunds($api_data);
      $this->handleResponse($api_data, $result, [ResultCode::RECEIVED]);
    }
    catch (AdyenException $e) {
      $this->handleException($e, $api_data ?? []);
    }

    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);
    if ($new_refunded_amount->lessThan($payment->getAmount())) {
      $payment->setState('partially_refunded');
    }
    else {
      $payment->setState('refunded');
    }

    $payment->setRefundedAmount($new_refunded_amount);
    $payment->save();
  }

  /**
   * Provide the current request uri.
   *
   * @return string
   *   The current uri.
   */
  protected function getCurrentUri(): string {
    // @todo url what would be an appropriate redirect url?
    // Do we need a new route or can we redirect to an existing checkout route?
    // We are using the Native 3ds2 component, so in most cases,
    // the redirect does not come into play.
    // Only a few specific payment processors perform redirect.
    return $this->requestStack->getCurrentRequest()->getUriForPath($this->requestStack->getCurrentRequest()->getPathInfo());
  }

  /**
   * Dispatch the api data for possible modification.
   *
   * @param string $event_type
   *   The event type.
   * @param array $data
   *   The data.
   * @param \Drupal\commerce_payment\Entity\PaymentInterface|null $payment
   *   The payment.
   *
   * @return array
   *   The api data.
   */
  protected function dispatchApiData(string $event_type, array $data, ?PaymentInterface $payment = NULL): array {
    // Add metadata and extra api data where required.
    $event = new ApiDataEvent($event_type, $data, $this->getOrder(), $payment);
    $this->eventDispatcher->dispatch($event, AdyenCCEvents::API_DATA);
    return $event->getApiData();
  }

  /**
   * Get generic order api data.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return array
   *   The api data.
   */
  protected function getOrderApiData(OrderInterface $order): array {
    $amount = $order->getBalance();
    $currency_code = $amount->getCurrencyCode();
    $api_data = [
      'merchantAccount' => $this->configuration['merchant_account'],
      'channel' => 'web',

      'shopperEmail' => $order->getCustomer()->getEmail() ?? $order->getEmail(),
      'shopperIP' => $this->requestStack->getCurrentRequest()->getClientIp(),
      'shopperReference' => $order->getCustomer()->uuid() ?? 'o-' . $order->uuid(),
      'shopperInteraction' => 'Ecommerce',

      'amount' => [
        'currency' => $currency_code,
        'value' => AdyenHelper::getMinorUnits($amount),
      ],
      'returnUrl' => $this->getCurrentUri(),
      'reference' => $order->id(),
    ];
    $api_data['applicationInfo'] = [
      'externalPlatform' => [
        'name' => 'Drupal Commerce',
        'version' => $this->getVersion(),
        'integrator' => 'Centarro',
      ],
      'merchantApplication' => [
        'name' => 'Drupal Commerce',
        'version' => $this->getVersion(),
      ],
    ];
    return $api_data;
  }

  /**
   * {@inheritDoc}
   *
   * @throws \Adyen\AdyenException
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  public function createPaymentSession(): array {
    $order = $this->getOrder();

    $session = $this->privateTempStore->get('adyen-session-' . $order->uuid());

    if (empty($session) || (($session['expiration'] ?? 0) < time())) {
      $service = new Checkout($this->getApi());
      $api_data = $this->getOrderApiData($order);

      $api_data = $this->dispatchApiData(EventType::SESSION, $api_data);
      try {
        $result = $service->sessions($api_data);
        $session = [
          'expiration' => strtotime('now +1 hour'),
          'data' => $result,
        ];
        $this->privateTempStore->set('adyen-session-' . $order->uuid(), $session);
      }
      catch (AdyenException $e) {
        $this->handleException($e, $api_data ?? []);
      }
    }
    return $session['data'];
  }

  /**
   * Deletes the Adyen session.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  protected function deletePaymentSession(): void {
    $this->privateTempStore->delete('adyen-session-' . $this->getOrder()->uuid());
    $this->privateTempStore->delete('payment-information-' . $this->getOrder()->uuid());
  }

  /**
   * {@inheritDoc}
   */
  public function authorize(PaymentInterface $payment): void {
    $order = $this->getOrder();
    $api_data = $this->getOrderApiData($order);
    $payment_method = $payment->getPaymentMethod();

    $api_data['paymentMethod'] = [
      'type' => 'scheme',
      'storedPaymentMethodId' => $payment_method->getRemoteId(),
    ];
    $api_data['shopperInteraction'] = 'ContAuth';
    $api_data['recurringProcessingModel'] = 'CardOnFile';

    $api_data = $this->dispatchApiData(EventType::AUTHORIZE, $api_data);

    try {
      $service = new Checkout($this->getApi());
      $result = $service->payments($api_data);
      $this->handleResponse($api_data, $result, [ResultCode::AUTHORIZED]);
      $next_state = $this->configuration['capture'] ? 'completed' : 'authorization';
      $payment->setState($next_state);
      $payment->setRemoteId($result['pspReference']);
      // @todo Find out how long an authorization is valid, set its expiration.
      $payment->save();
    }
    catch (AdyenException $e) {
      $this->handleException($e, $api_data ?? []);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function verify3ds2(array $data): void {
    $adyen_cc_data_3ds2_value = $data['adyen_cc_data_3ds2'];
    assert(!empty($adyen_cc_data_3ds2_value), 'We do not have a valid payload.');
    $adyen_cc_data_3ds2 = json_decode($adyen_cc_data_3ds2_value, TRUE, 512, JSON_THROW_ON_ERROR);

    $api_data = [
      'details' => $adyen_cc_data_3ds2['details'],
    ];
    $api_data = $this->dispatchApiData(EventType::AUTHENTICATE_3DS2_VERIFY, $api_data);

    try {
      $service = new Checkout($this->getApi());
      $result = $service->paymentsDetails($api_data);
      $this->handleResponse($api_data, $result, [ResultCode::AUTHORIZED]);
      $payment_method = $result['additionalData']['paymentMethod'] ?? $result['paymentMethod']['brand'] ?? NULL;
      if (!empty($payment_method)) {
        $payment_information['card_type'] = $payment_method;
      }
      else {
        $payment_information['card_type'] = $this->t('Unknown', [], ['context' => 'commerce_adyen_cc']);
        $clean_array = $result;
        unset($clean_array['additionalData']['recurring.recurringDetailReference'], $clean_array['additionalData']['recurring.shopperReference']);
        $response_data = json_encode($clean_array);
        $this->logger->error('Unable to find paymentMethod in response: ' . $response_data);
      }
      if (!empty($result['additionalData']['cardSummary'])) {
        $payment_information['card_number'] = $result['additionalData']['cardSummary'];
      }
      else {
        throw new InvalidConfigurationException('Expected additionalData property cardSummary not provided. It appears your Adyen account is not properly configured.');
      }
      $expiration = $result['additionalData']['expiryDate'] ?? NULL;
      if (!empty($expiration)) {
        $expiration_parts = explode('/', $expiration);
        $payment_information['card_exp_month'] = $expiration_parts[0];
        $payment_information['card_exp_year'] = $expiration_parts[1];
      }
      else {
        throw new InvalidConfigurationException('Expected additionalData property expiryDate not provided. It appears your Adyen account is not properly configured.');
      }
      $remote_id = $result['additionalData']['recurring.recurringDetailReference'] ?? NULL;
      if (empty($remote_id)) {
        throw new InvalidConfigurationException('Expected additionalData property recurring.recurringDetailReference not provided. It appears your Adyen account is not properly configured.');
      }
      $payment_information['remote_id'] = $remote_id;
      $this->setPaymentInformation($payment_information);
    }
    catch (AdyenException $e) {
      $this->handleException($e, $api_data ?? []);
    }
  }

  /**
   * Get the Adyen api client.
   *
   * @return \Adyen\Client
   *   The api client.
   *
   * @throws \Adyen\AdyenException
   */
  public function getApi(): Client {
    if ($this->api === NULL) {
      $this->api = new Client();
      $this->api->setApplicationName('Drupal Commerce Adyen Card Component');
      $api_key = $this->configuration['api_key'];
      $this->api->setXApiKey($api_key);
      $environment = ($this->getMode() === 'test') ? Environment::TEST : Environment::LIVE;
      $this->api->setEnvironment($environment, $this->configuration['live_endpoint_url_prefix']);
      $timeout = $this->configuration['timeout'];
      $this->api->setTimeout($timeout);
    }
    return $this->api;
  }

  /**
   * Get the version of the module.
   *
   * @return string
   *   The version.
   */
  public function getVersion(): string {
    if ($this->version === NULL) {
      try {
        $info = $this->moduleExtensionList->getExtensionInfo('commerce_adyen_cc');
        $version = $info->version ?? NULL;
        if ($version !== NULL) {
          $this->version = $version;
        }
        else {
          $this->version = 'dev';
        }
      }
      catch (\Throwable $e) {
        $this->version = 'unknown';
      }
    }
    return $this->version;
  }

  /**
   * Get the order.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface
   *   The order.
   */
  protected function getOrder(): OrderInterface {
    if ($this->order === NULL) {
      $this->order = $this->routeMatch->getParameter('commerce_order');
    }
    return $this->order;
  }

  /**
   * {@inheritDoc}
   */
  public function getBillingAddressAllowedCountries(): array {
    return $this->getOrder()->getStore()->getBillingCountries();
  }

  /**
   * Get the payment information.
   *
   * @return array
   *   The payment data.
   */
  protected function getPaymentInformation(): array {
    return $this->privateTempStore->get('payment-information-' . $this->getOrder()->uuid()) ?? [];
  }

  /**
   * Set the payment information.
   *
   * @param array $payment_information
   *   The payment information.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  protected function setPaymentInformation(array $payment_information): void {
    $this->privateTempStore->set('payment-information-' . $this->getOrder()->uuid(), $payment_information);
  }

  /**
   * Get the billing address in Adyen naming convention and structure.
   *
   * @param \Drupal\profile\Entity\ProfileInterface $billing_profile
   *   The billing profile.
   *
   * @return array
   *   The address.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  protected function getBillingAddress(ProfileInterface $billing_profile): array {
    /** @var \Drupal\address\Plugin\Field\FieldType\AddressItem $address */
    $address = $billing_profile->get('address')->first();
    return [
      'street' => $address->getAddressLine1(),
      'houseNumberOrName' => $address->getAddressLine2(),
      'postalCode' => $address->getPostalCode(),
      'city' => $address->getLocality(),
      'stateOrProvince' => $address->getAdministrativeArea(),
      'country' => $address->getCountryCode(),
    ];
  }

  /**
   * Get the cardholder name.
   *
   * @param \Drupal\profile\Entity\ProfileInterface $billing_profile
   *   The billing profile.
   *
   * @return string
   *   The name.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  protected function getHolderName(ProfileInterface $billing_profile): string {
    /** @var \Drupal\address\Plugin\Field\FieldType\AddressItem $address */
    $address = $billing_profile->get('address')->first();
    return trim(implode(' ', [
      $address->getGivenName(),
      $address->getFamilyName(),
    ]));
  }

  /**
   * Translates Adyen exceptions into Commerce exceptions.
   *
   * @param \Adyen\AdyenException $exception
   *   The Adyen exception.
   * @param array $request
   *   The Adyen request.
   *
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   *   The Commerce exception.
   */
  protected function handleException(AdyenException $exception, array $request): void {
    $this->log($request, Error::decodeException($exception));
    throw new InvalidResponseException($exception->getMessage());
  }

  /**
   * Process Adyen response.
   *
   * @param array $request
   *   The Adyen request.
   * @param array $response
   *   The Adyen response.
   * @param array $expected_result_codes
   *   The expected result codes.
   */
  protected function handleResponse(array $request, array $response, array $expected_result_codes): void {
    $result_code = $response['resultCode'] ?? $response['status'] ?? 'unknown';
    $this->log($request, $response);
    if (in_array($result_code, $expected_result_codes, TRUE)) {
      return;
    }
    if ($response['resultCode'] === ResultCode::REFUSED) {
      $message = $response['refusalReason'] ?? 'Unknown';
      $refusal_reason_code = $response['refusalReasonCode'] ?? '';
      if (!empty($refusal_reason_code)) {
        $message .= ' (' . $refusal_reason_code . ')';
      }
      throw new SoftDeclineException(trim($message));
    }

    // Throw a fallback exception for everything else.
    throw new InvalidRequestException('Unexpected Result Code: ' . $result_code);
  }

  /**
   * Log the request.
   *
   * @param array $request
   *   The request data.
   * @param array $response_or_exception
   *   The response (or exception) data.
   */
  protected function log(array $request, array $response_or_exception): void {
    try {
      $order_id = $this->getOrder()->id();
      $data = json_encode([
        'request' => $request,
        'response' => $response_or_exception,
      ], JSON_PRETTY_PRINT | JSON_THROW_ON_ERROR) ?? '{}';
      if ($this->configuration['save_api_requests']) {
        $date = $this->dateFormatter->format(time(), 'custom', 'Y-m-d');
        $time = $this->dateFormatter->format(time(), 'custom', 'H-i-s');
        $path = 'private://commerce-adyen-cc/' . $date;
        $file_name = $date . '-' . $time . '--' . $order_id . '.json';
        $uri = $path . '/' . $file_name;
        $this->fileSystem->prepareDirectory($path, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
        $this->fileSystem->saveData($data, $uri);
      }
      if ($this->configuration['log_api_requests']) {
        $this->logger->debug($data);
      }
    }
    catch (\Throwable $e) {

    }
  }

  /**
   * {@inheritDoc}
   */
  public function tokenize(array $payment_details, ProfileInterface $billing_profile): void {
    $required_keys = ['adyen_cc_data'];

    // Use PaymentGatewayException instead of InvalidArgumentException
    // to handle missing data items.
    foreach ($required_keys as $required_key) {
      if (empty($payment_details[$required_key])) {
        throw new PaymentGatewayException(sprintf('In WebComponent::tokenize(), $payment_details must contain the %s key.', $required_key));
      }
    }

    $payment_information = $this->getPaymentInformation();
    $order = $this->getOrder();

    $adyen_cc_data_value = $payment_details['adyen_cc_data'];
    assert(!empty($adyen_cc_data_value), 'We do not have a valid payload.');
    $adyen_cc_data = json_decode($adyen_cc_data_value, TRUE, 512, JSON_THROW_ON_ERROR);
    $billing_address = $this->getBillingAddress($billing_profile);
    $holder_name = $this->getHolderName($billing_profile);

    $api_data = $this->getOrderApiData($order);

    $api_data['paymentMethod'] = $adyen_cc_data['paymentMethod'];
    $api_data['paymentMethod']['holderName'] = $holder_name;
    $api_data['billingAddress'] = $billing_address;
    $api_data['browserInfo'] = $adyen_cc_data['browserInfo'];

    if (empty($api_data['shopper_email']) && !empty($payment_details['customer_email'])) {
      $api_data['shopperEmail'] = $payment_details['customer_email'];
    }

    $api_data['authenticationData'] = [
      'threeDSRequestData' => [
        'nativeThreeDS' => 'preferred',
      ],
    ];
    $api_data['threeDS2RequestData'] = [
      'deviceChannel' => 'browser',
    ];
    if (!$this->configuration['capture']) {
      $api_data['additionalData'] = [
        'manualCapture' => TRUE,
      ];
    }
    // We perform a zero dollar authorization, so we can:
    // 1) have the card validation kick in
    // 2) get a token back, allowing authorization at a later time. (checkout)
    // The 3ds2 component displays $0 during account verification.
    // We optionally add the additionalAmount so that the 3ds2 component
    // will display the proper amount being requested.
    // In this case, Adyen performs an auth+cancel for the additional amount,
    // rather than a $0 auth.
    // Note: some payments do not support $0 auth, and will cause an auth+cancel
    // regardless if this setting.
    // See: https://docs.adyen.com/payment-methods/cards/bin-data-and-card-verification
    // IRT additionalAmount.
    if ($this->configuration['include_additional_amount']) {
      $api_data['additionalAmount'] = $api_data['amount'];
    }
    $api_data['amount']['value'] = 0;

    $api_data['storePaymentMethod'] = TRUE;
    $api_data['recurringProcessingModel'] = 'CardOnFile';
    $api_data['origin'] = $this->getCurrentUri();
    foreach ($order->getItems() as $item) {
      $api_data['lineItems'][] = [
        'quantity' => (int) $item->getQuantity(),
        'amountExcludingTax' => AdyenHelper::getMinorUnits($item->getTotalPrice()),
        'description' => $item->label(),
      ];
    }

    $api_data = $this->dispatchApiData(EventType::AUTHORIZE_ZERO_DOLLAR_3DS2_NOT_REQUIRED, $api_data);

    try {
      $service = new Checkout($this->getApi());
      $result = $service->payments($api_data);
      $this->handleResponse($api_data, $result, [
        ResultCode::AUTHORIZED,
        ResultCode::IDENTIFY_SHOPPER,
        ResultCode::CHALLENGE_SHOPPER,
      ]);

      switch ($result['resultCode']) {
        case ResultCode::AUTHORIZED:
          $remote_id = $result['additionalData']['recurring.recurringDetailReference'] ?? NULL;
          if (empty($remote_id)) {
            throw new InvalidConfigurationException('Expected additionalData property recurring.recurringDetailReference not provided. It appears your Adyen account is not properly configured.');
          }
          $payment_information['remote_id'] = $remote_id;
          $payment_information['payment_data'] = $result['threeDSPaymentData'];

          $payment_method = $result['additionalData']['paymentMethod'] ?? $result['paymentMethod']['brand'] ?? NULL;
          if (!empty($payment_method)) {
            $payment_information['card_type'] = $payment_method;
          }
          else {
            $payment_information['card_type'] = $this->t('Unknown', [], ['context' => 'commerce_adyen_cc']);
            $clean_array = $result;
            unset($clean_array['threeDSPaymentData']);
            $response_data = json_encode($clean_array);
            $this->logger->error('Unable to find paymentMethod in response: ' . $response_data);
          }
          if (!empty($result['additionalData']['cardSummary'])) {
            $payment_information['card_number'] = $result['additionalData']['cardSummary'];
          }
          else {
            throw new InvalidConfigurationException('Expected additionalData property cardSummary not provided. It appears your Adyen account is not properly configured.');
          }
          $expiration = $result['additionalData']['expiryDate'] ?? NULL;
          if (!empty($expiration)) {
            $expiration_parts = explode('/', $expiration);
            $payment_information['card_exp_month'] = $expiration_parts[0];
            $payment_information['card_exp_year'] = $expiration_parts[1];
          }
          else {
            throw new InvalidConfigurationException('Expected additionalData property expiryDate not provided. It appears your Adyen account is not properly configured.');
          }
          $this->setPaymentInformation($payment_information);
          break;

        case ResultCode::CHALLENGE_SHOPPER:
        case ResultCode::IDENTIFY_SHOPPER:
          $payment_information = $this->getPaymentInformation();
          $payment_information['payment_data'] = $result['action']['paymentData'];
          $this->setPaymentInformation($payment_information);
          throw new ThreeDS2Exception('3ds2 required.', 0, NULL, $result);
      }
    }
    catch (AdyenException $e) {
      $this->handleException($e, $api_data ?? []);
    }
  }

}
