<?php

namespace Drupal\commerce_adyen_cc\Event;

use Drupal\commerce\EventBase;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;

/**
 * Defines the api data event.
 *
 * This enables other modules to add data and metadata to the
 * api call that will be sent to Adyen.
 *
 * @see \Drupal\commerce_adyen_cc\Event\AdyenCCEvents
 */
class ApiDataEvent extends EventBase {

  /**
   * The event type.
   *
   * @var string
   */
  protected $eventType;

  /**
   * The data.
   *
   * @var array
   */
  protected $apiData = [];

  /**
   * The order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * The payment.
   *
   * @var \Drupal\commerce_payment\Entity\PaymentInterface
   */
  protected $payment;

  /**
   * Constructs a new ApiDataEvent object.
   *
   * @param string $event_type
   *   The event type.
   * @param array $api_data
   *   The api data to submit to Adyen.
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param \Drupal\commerce_payment\Entity\PaymentInterface|null $payment
   *   The payment.
   *
   * @see \Drupal\commerce_adyen_cc\Enums\EventType
   */
  public function __construct(string $event_type, array $api_data, OrderInterface $order, ?PaymentInterface $payment = NULL) {
    $this->eventType = $event_type;
    $this->apiData = $api_data;
    $this->order = $order;
    $this->payment = $payment;
  }

  /**
   * The event type.
   *
   * @return string
   *   The event type.
   */
  public function getEventType(): string {
    return $this->eventType;
  }

  /**
   * Get the api data.
   *
   * @return array
   *   The api data.
   */
  public function getApiData(): array {
    return $this->apiData;
  }

  /**
   * The order.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface
   *   The order.
   */
  public function getOrder(): OrderInterface {
    return $this->order;
  }

  /**
   * Get the payment.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentInterface|null
   *   The payment.
   */
  public function getPayment(): ?PaymentInterface {
    return $this->payment;
  }

}
