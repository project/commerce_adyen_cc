<?php

namespace Drupal\commerce_adyen_cc\Event;

/**
 * Defines events for the Commerce Adyen Card Component module.
 */
class AdyenCCEvents {

  /**
   * Name of the event fired to add additional api data.
   *
   * This event is triggered when an api call is going to be invoked. It allows
   * subscribers to add additional data and metadata.
   *
   * @Event
   *
   * @see \Drupal\commerce_adyen_cc\Event\ApiDataEvent
   */
  public const API_DATA = 'commerce_adyen_cc.api_data';

}
