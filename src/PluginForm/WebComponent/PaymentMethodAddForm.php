<?php

namespace Drupal\commerce_adyen_cc\PluginForm\WebComponent;

use Adyen\Environment;
use Drupal\commerce_adyen_cc\Ajax\HandleAdyenActionCommand;
use Drupal\commerce_adyen_cc\Ajax\HandleAdyenErrorCommand;
use Drupal\commerce_adyen_cc\Ajax\HandleAdyenSuccessCommand;
use Drupal\commerce_adyen_cc\Exception\InvalidConfigurationException;
use Drupal\commerce_adyen_cc\Exception\ThreeDS2Exception;
use Drupal\commerce_payment\Exception\DeclineException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentMethodAddForm as BasePaymentMethodAddForm;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the Web Component payment method add form.
 */
class PaymentMethodAddForm extends BasePaymentMethodAddForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['#attached']['library'][] = 'commerce_payment/payment_method_form';
    $form['#tree'] = TRUE;
    $form['payment_details'] = $this->buildAdyenForm($form['payment_details'], $form_state);

    $form['verify_payment'] = [
      '#type' => 'button',
      '#name' => 'verify_payment',
      '#value' => $this->t('Verify'),
      '#ajax' => [
        'callback' => [$this, 'ajaxVerify'],
      ],
      '#attributes' => [
        'class' => ['js-hide'],
      ],
    ];

    return $form;
  }

  /**
   * Builds the Adyen Payment form.
   *
   * @param array $element
   *   The target element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   *
   * @return array
   *   The built Adyen payment form.
   */
  public function buildAdyenForm(array $element, FormStateInterface $form_state): array {
    /** @var \Drupal\commerce_adyen_cc\Plugin\Commerce\PaymentGateway\WebComponentInterface $plugin */
    $plugin = $this->plugin;

    $session = $plugin->createPaymentSession();

    $environment = ($plugin->getMode() === 'test') ? Environment::TEST : Environment::LIVE;
    $element['#attached']['library'][] = 'commerce_adyen_cc/web_component';
    $element['#attached']['drupalSettings']['commerceAdyenCC'] = [
      'environment' => $environment,
      'clientKey' => $plugin->getConfiguration()['client_key'],
      'sessionData' => $session['sessionData'],
      'id' => $session['id'],
      'billingAddressAllowedCountries' => $plugin->getBillingAddressAllowedCountries(),
    ];

    $element['#attributes']['class'][] = 'adyen-cc-form';
    // Populated by JS.
    $element['adyen_cc_data'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'class' => ['adyen-cc-data'],
      ],
    ];
    $element['adyen_cc_data_3ds2'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'class' => ['adyen-cc-data-3ds2'],
      ],
    ];
    $element['card_container'] = [
      '#type' => 'container',
      '#id' => 'card-container',
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  protected function validatePayPalForm(array &$element, FormStateInterface $form_state) {
    // The JS library performs its own validation.
  }

  /**
   * {@inheritdoc}
   */
  protected function validateCreditCardForm(array &$element, FormStateInterface $form_state) {
    // The JS library performs its own validation.
  }

  /**
   * {@inheritdoc}
   */
  public function submitPayPalForm(array $element, FormStateInterface $form_state) {
    // The payment gateway plugin will process the submitted payment details.
  }

  /**
   * {@inheritdoc}
   */
  public function submitCreditCardForm(array $element, FormStateInterface $form_state) {
    // The payment gateway plugin will process the submitted payment details.
  }

  /**
   * Ajax callback.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax response.
   */
  public function ajaxVerify(array $form, FormStateInterface $form_state): AjaxResponse {
    $response = new AjaxResponse();
    try {
      /** @var \Drupal\commerce_adyen_cc\Plugin\Commerce\PaymentGateway\WebComponentInterface $payment_gateway_plugin */
      $payment_gateway_plugin = $this->plugin;

      $payment_information_values = $form_state->getValue([
        'payment_information',
        'add_payment_method',
      ]);
      $payment_details = $payment_information_values['payment_details'];

      if (empty($payment_details['adyen_cc_data_3ds2'])) {
        $customer_email = $form_state->getValue([
          'contact_information',
          'email',
        ]);
        if (!empty($customer_email)) {
          // If this is an anonymous order, the email might not be set yet.
          // Pass this along for a fallback.
          $payment_details['customer_email'] = $customer_email;
        }

        /** @var \Drupal\commerce\Plugin\Commerce\InlineForm\EntityInlineFormInterface $inline_form */
        $inline_form = $form['payment_information']['add_payment_method']['billing_information']['#inline_form'];
        /** @var \Drupal\profile\Entity\ProfileInterface $billing_profile */
        $billing_profile = $inline_form->getEntity();
        $payment_gateway_plugin->tokenize($payment_details, $billing_profile);
      }
      else {
        $payment_gateway_plugin->verify3ds2($payment_details);
      }
      $response->addCommand(new HandleAdyenSuccessCommand());
    }
    catch (ThreeDS2Exception $e) {
      $response->addCommand(new HandleAdyenActionCommand($e->getData()));
    }
    catch (DeclineException $e) {
      $this->logger->warning($e->getMessage());
      $response->addCommand(new HandleAdyenErrorCommand($this->t('We encountered an error processing your payment method. Please verify your details and try again.')));
    }
    catch (PaymentGatewayException $e) {
      $this->logger->error($e->getMessage());
      $response->addCommand(new HandleAdyenErrorCommand($this->t('We encountered an unexpected error processing your payment method. Please try again later.')));
    }
    catch (InvalidConfigurationException $e) {
      $this->logger->critical($e->getMessage());
      $response->addCommand(new HandleAdyenErrorCommand($this->t('We encountered an unexpected error.')));
    }
    catch (\Throwable $e) {
      $this->logger->error($e->getMessage());
      $response->addCommand(new HandleAdyenErrorCommand($this->t('We encountered an unexpected error.')));
    }

    return $response;
  }

}
