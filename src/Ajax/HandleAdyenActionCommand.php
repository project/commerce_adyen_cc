<?php

namespace Drupal\commerce_adyen_cc\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * AJAX command for handling an Adyen action.
 */
class HandleAdyenActionCommand implements CommandInterface {

  /**
   * The action data.
   *
   * @var array
   */
  protected $data;

  /**
   * Constructs a new HandleAdyenActionCommand object.
   *
   * @param array $data
   *   The adyen action data.
   */
  public function __construct(array $data) {
    $this->data = $data;
  }

  /**
   * Return an array to be run through json_encode and sent to the client.
   */
  public function render() {
    return [
      'command' => 'handleAdyenAction',
      'data' => $this->data,
    ];
  }

}
