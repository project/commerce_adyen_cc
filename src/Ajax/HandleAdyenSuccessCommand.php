<?php

namespace Drupal\commerce_adyen_cc\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * AJAX command for handling an Adyen success.
 */
class HandleAdyenSuccessCommand implements CommandInterface {

  /**
   * Constructs a new HandleAdyenSuccessCommand object.
   */
  public function __construct() {

  }

  /**
   * Return an array to be run through json_encode and sent to the client.
   */
  public function render() {
    return [
      'command' => 'handleAdyenSuccess',
    ];
  }

}
