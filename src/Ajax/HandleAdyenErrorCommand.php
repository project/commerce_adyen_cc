<?php

namespace Drupal\commerce_adyen_cc\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * AJAX command for handling an Adyen error.
 */
class HandleAdyenErrorCommand implements CommandInterface {

  /**
   * The error message.
   *
   * @var string
   */
  protected $message;

  /**
   * Constructs a new HandleAdyenErrorCommand object.
   *
   * @param string $message
   *   The adyen response error message.
   */
  public function __construct(string $message) {
    $this->message = $message;
  }

  /**
   * Return an array to be run through json_encode and sent to the client.
   */
  public function render() {
    return [
      'command' => 'handleAdyenError',
      'message' => $this->message,
    ];
  }

}
