/**
 * @file
 * Defines behaviors for the Adyen Card Component payment method form.
 */

(($, Drupal, drupalSettings, AdyenCheckout) => {
  /**
   * Attaches the commerceAdyenCC behavior.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the commerceAdyenCC behavior.
   */
  Drupal.behaviors.commerceAdyenCC = {
    attach(context) {
      const $form = $(
        once('adyen-cc-attach', '.adyen-cc-form', context),
      ).closest('form');

      if ($form.length === 0) {
        return;
      }
      const $submit = $form.find(
        ':input[data-drupal-selector=edit-actions-next]',
      );
      const $verify = $form.find(':input[name=verify_payment]');

      $submit.attr('disabled', true);
      $verify.attr('disabled', true);

      // Form submit.
      $form.on('submit.adyen_cc', (event, options) => {
        // Disable the submit button to prevent repeated clicks.
        $form
          .find('[data-drupal-selector=edit-actions-next]')
          .prop('disabled', true);
        // Disable other form buttons, so that they don't cause an inadvertent
        // form submission or ajax request.
        Drupal.behaviors.commerceAdyenCC.formButtons = $form.find(
          ':submit:not([disabled]):not([data-drupal-selector=edit-actions-next]):not([name=verify_payment])',
        );
        Drupal.behaviors.commerceAdyenCC.formButtons.prop('disabled', true);
        options = options || {};
        if (options.populated) {
          return;
        }
        event.preventDefault();

        $verify.trigger('mousedown');

        // Prevent the form from submitting with the default action.
        return false;
      });

      const waitForSdk = setInterval(async () => {
        if (
          typeof AdyenCheckout !== 'undefined' &&
          typeof Drupal.commerceAdyenWebComponent !== 'undefined'
        ) {
          const commerceAdyenCheckout = await Drupal.commerceAdyenWebComponent(
            $form,
            drupalSettings.commerceAdyenCC,
          );
          $form.data('adyen-cc', commerceAdyenCheckout);
          clearInterval(waitForSdk);
        }
      }, 100);
    },
    detach(context, settings, trigger) {
      // Detaching on the wrong trigger will clear the Adyen form
      // on #ajax.
      if (trigger !== 'unload') {
        return;
      }
      const $form = $('.adyen-cc-form', context).closest('form');
      if ($form.length === 0) {
        return;
      }
      const $submit = $form.find(
        ':input[data-drupal-selector=edit-actions-next]',
      );

      once.remove('adyen-cc-attach', $form, context);
      $submit.prop('disabled', false);
    },
  };

  $.extend(
    Drupal.theme,
    /** @lends Drupal.theme */ {
      commerceAdyenCCError(message) {
        return $(
          `<div role="alert"><div class="messages messages--error">${message}</div></div>`,
        );
      },
    },
  );
})(jQuery, Drupal, drupalSettings, window.AdyenCheckout);
