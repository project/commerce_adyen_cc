/**
 * @file
 * Defines behaviors for the Braintree hosted fields payment method form.
 */

(($, Drupal, drupalSettings, AdyenCheckout) => {
  /**
   * Ajax command to handle the adyen payment action.
   *
   * @param {Drupal.Ajax} ajax
   *   {@link Drupal.Ajax} object created by {@link Drupal.ajax}.
   * @param {object} response
   *   JSON response from the Ajax request.
   * @param {number} [status]
   *   XMLHttpRequest status.
   */
  Drupal.AjaxCommands.prototype.handleAdyenAction = (
    ajax,
    response,
    // eslint-disable-next-line no-unused-vars
    status,
  ) => {
    const { data } = response;
    const $form = $('.adyen-cc-form').closest('form');
    if (Drupal.behaviors.commerceAdyenCC.formButtons !== null) {
      Drupal.behaviors.commerceAdyenCC.formButtons.prop('disabled', false);
      Drupal.behaviors.commerceAdyenCC.formButtons = null;
    }
    const $submit = $form.find(
      ':input[data-drupal-selector=edit-actions-next]',
    );
    $submit.attr('disabled', true);
    const $verify = $form.find(':input[name=verify_payment]');
    $verify.attr('disabled', true);
    Drupal.commerceAdyenWebComponent.cardComponent.handleAction(data.action);
    $('div[data-drupal-selector=card-container]')[0].scrollIntoView();
  };

  /**
   * Ajax command to handle the adyen payment error.
   *
   * @param {Drupal.Ajax} ajax
   *   {@link Drupal.Ajax} object created by {@link Drupal.ajax}.
   * @param {object} response
   *   JSON response from the Ajax request.
   * @param {number} [status]
   *   XMLHttpRequest status.
   */
  // eslint-disable-next-line no-unused-vars
  Drupal.AjaxCommands.prototype.handleAdyenError = (ajax, response, status) => {
    const { message } = response;
    const $form = $('.adyen-cc-form').closest('form');
    if (Drupal.behaviors.commerceAdyenCC.formButtons !== null) {
      Drupal.behaviors.commerceAdyenCC.formButtons.prop('disabled', false);
      Drupal.behaviors.commerceAdyenCC.formButtons = null;
    }
    const $verify = $form.find(':input[name=verify_payment]');
    $verify.attr('disabled', false);
    $('.messages--error', $form).remove();
    const $submit = $form.find(
      ':input[data-drupal-selector=edit-actions-next]',
    );
    $submit.attr('disabled', false);
    if (Drupal.behaviors.commerceAdyenCC.formButtons !== null) {
      Drupal.behaviors.commerceAdyenCC.formButtons.prop('disabled', false);
      Drupal.behaviors.commerceAdyenCC.formButtons = null;
    }
    $form.prepend(Drupal.theme('commerceAdyenCCError', message));
    $('html, body').animate(
      {
        scrollTop: $('[role="alert"]').offset().top - 200,
      },
      1000,
    );
    $('.adyen-cc-data-3ds2')[0].value = '';
    Drupal.commerceAdyenWebComponent.cardComponent.remount();
  };

  /**
   * Ajax command to handle the adyen payment success.
   *
   * @param {Drupal.Ajax} ajax
   *   {@link Drupal.Ajax} object created by {@link Drupal.ajax}.
   * @param {object} response
   *   JSON response from the Ajax request.
   * @param {number} [status]
   *   XMLHttpRequest status.
   */
  Drupal.AjaxCommands.prototype.handleAdyenSuccess = (
    // eslint-disable-next-line no-unused-vars
    ajax,
    // eslint-disable-next-line no-unused-vars
    response,
    // eslint-disable-next-line no-unused-vars
    status,
  ) => {
    const $form = $('.adyen-cc-form').closest('form');
    const $submit = $form.find(
      ':input[data-drupal-selector=edit-actions-next]',
    );
    $submit.attr('disabled', false);
    const $verify = $form.find(':input[name=verify_payment]');
    $verify.attr('disabled', true);
    $form.trigger('submit', { populated: true });
  };

  Drupal.commerceAdyenWebComponent = async ($form, settings) => {
    const configuration = {
      environment: settings.environment,
      clientKey: settings.clientKey,
      analytics: {
        enabled: false,
      },
      session: {
        id: settings.id,
        sessionData: settings.sessionData,
      },
      onError: (error, component) => {
        // eslint-disable-next-line no-console
        console.error(error.name, error.message, error.stack, component);
        $('.messages--error', $form).remove();
        $form.prepend(Drupal.theme('commerceAdyenCCError', error.message));
        $('html, body').animate(
          {
            scrollTop: $('[role="alert"]').offset().top - 200,
          },
          1000,
        );
      },
      // eslint-disable-next-line no-unused-vars
      onChange: (result, component) => {
        const $verify = $form.find(':input[name=verify_payment]');
        const $submit = $form.find(
          ':input[data-drupal-selector=edit-actions-next]',
        );
        if (result.isValid) {
          $('.adyen-cc-data')[0].value = JSON.stringify(result.data);
          $verify.attr('disabled', false);
          $submit.attr('disabled', false);
        } else {
          $verify.attr('disabled', true);
          $submit.attr('disabled', true);
        }
      },
      // eslint-disable-next-line no-unused-vars
      onAdditionalDetails: (result, component) => {
        $('.adyen-cc-data-3ds2')[0].value = JSON.stringify(result.data);
        const $verify = $form.find(':input[name=verify_payment]');
        const $submit = $form.find(
          ':input[data-drupal-selector=edit-actions-next]',
        );
        $verify.attr('disabled', false);
        $submit.attr('disabled', true);
        Drupal.behaviors.commerceAdyenCC.formButtons = $form.find(
          ':submit:not([disabled]):not([data-drupal-selector=edit-actions-next]):not([name=verify_payment])',
        );
        Drupal.behaviors.commerceAdyenCC.formButtons.prop('disabled', true);
        $verify[0].dispatchEvent(new Event('mousedown'));
      },
      // eslint-disable-next-line no-unused-vars
      onActionHandled: (result, component) => {
        // eslint-disable-next-line no-console
        console.log(JSON.stringify(result));
      },
      paymentMethodsConfiguration: {
        card: {
          autoFocus: false,
        },
      },
    };

    const checkout = await AdyenCheckout(configuration);
    Drupal.commerceAdyenWebComponent.checkout = checkout;
    Drupal.commerceAdyenWebComponent.cardComponent = checkout
      .create('card')
      .mount('#card-container');

    return this;
  };
})(jQuery, Drupal, drupalSettings, window.AdyenCheckout);
