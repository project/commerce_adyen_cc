# Commerce Adyen Card Component

This project integrates Commerce Core with Adyen's web integration API,
facilitating the secure collection of credit card data through iframes and the
processing of payments through Adyen's REST API. The project supports a
custom 3D Secure 2 component as required for web integrations.

This project is differentiated from the existing Commerce Adyen project in that
it only supports Commerce 2.x+ and does not intend to support the legacy hosted
payment page API.

## REQUIREMENTS

This module requires the following modules:

- [Commerce](https://drupal.org/project/commerce)

## INSTALLATION

Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/docs/extending-drupal/installing-modules for
further information.

## CONFIGURATION

_**Important!**_ This module requires specific configuration in your Adyen
account. This configuration must be done in your test _**AND**_ live
accounts.

In your Adyen Account, go to "Developer > Additional Data".

### Card additional data

Under the "Card" section,
1. Ensure "Card Summary" is enabled.
2. Ensure "Card Expiry" is enabled.

### Token additional data

Under the "Payment" section,
1. Ensure "Recurring details" is enabled.
2. Ensure "Recurring contract information" is enabled.

### Capture Delay

In your Adyen Account, Switch to your merchant account, and go to
"Settings > Account settings > General > Capture delay".

1. Ensure that it is **NOT** set to "Manual Capture". (You can still select
   the "Authorize Only" option in the Payment Gateway configuration if you would
   like to use Manual Capture.)

## KNOWN ISSUES AND LIMITATIONS

[Issue Tracker](https://www.drupal.org/project/issues/commerce_adyen_cc?version=1.0.x)

As this is the initial implementation, not all errors, exceptions, and edge
cases may be handled properly. This will be enhanced over time.

### Refused test payments

It's possible that the use of local development environments or other testing
environment configurations may result in test payments being refused. This is
particularly noticeable when using test card numbers that require 3D Secure
authentication.

[3DS test credit card numbers](https://docs.adyen.com/development-resources/testing/test-card-numbers#test-3d-secure-2-authentication)

To avoid refusals, you can edit your risk profile in the Adyen UI to whitelist
payment attempts from your email address. For example:

1. Log in to your Adyen account.
2. Click on `Risk` in the left sidebar.
3. When that expands, click on `Risk profiles` in the submenu.
4. Click on the prolfile name you want to edit. This may have `Parent` in it.
5. Click on the `Manual risk` tab.
6. Click on `Block and trust lists`.
7. Scroll down to `Shopper email` and click it to expand it.
8. Click the `See and edit referral list` link to navigate to a trust list.
9. Click the `Add item` button in the right column.
10. Input your email address, select the `Trust` action, and click `Apply`.

Now when you test a transaction using that email address and a 3DS test card
number, the card component should be replaced with a 3DS prompt in the iframe
where you will enter `password` to authenticate your transaction.
